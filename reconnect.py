import requests
import json

API_URL = "https://server.smartlead.ai/api/v1/email-accounts/reconnect-failed-email-accounts?api_key=YOUR-API-KEY-GOES-HERE"
headers = {
    "Content-Type": "application/json"
}

response = requests.post(API_URL, headers=headers, json={})
response_data = response.json()

OK = response_data.get('ok')
MESSAGE = response_data.get('message')

if OK == True:
    if MESSAGE == "Email account(s) added to the queue to reconnect. We will send you an email once completed.":
        print("Reconnected Smartlead accounts.")
    elif MESSAGE == "No failed email account found!":
        print("There are no disconnected accounts!")
    else:
        print(f"Unknown response from server: {MESSAGE}")
else:
    print("Error connecting to server.")
