![](https://i.imgur.com/w3COuyF.jpg)

# Smartlead Reconnection Script

### Purpose

This script will make an API call to Smartlead to bulk reconnect all disconnected accounts (Superwave or not) at the press of a button. You should receive a confirmation email from Smartlead if successful.

### Usage

After downloading the script you need to fetch your Smartlead API key from [your profile menu](https://app.smartlead.ai/app/settings/profile).

Run `python reconnect.py` in your terminal as needed (Note: You are required to have Python installed).

You can also set the script to run every 24 hours as a `cron` job.

### Warning

If you don't know what you're doing have someone who knows do it for you. You're solely responsible for your own actions.

